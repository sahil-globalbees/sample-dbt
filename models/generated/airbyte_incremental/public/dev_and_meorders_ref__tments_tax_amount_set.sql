{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref__tments_tax_amount_set_ab3') }}
select
    _airbyte_order_adjustments_hashid,
    shop_money,
    presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_tax_amount_set_hashid
from {{ ref('dev_and_meorders_ref__tments_tax_amount_set_ab3') }}
-- tax_amount_set at dev_and_meorders/refunds/order_adjustments/tax_amount_set from {{ ref('dev_and_meorders_refunds_order_adjustments') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

