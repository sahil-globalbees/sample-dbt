{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_fulfillments_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ adapter.quote('id') }},
    {{ adapter.quote('name') }},
    status,
    receipt,
    service,
    order_id,
    created_at,
    line_items,
    updated_at,
    location_id,
    tracking_url,
    tracking_urls,
    shipment_status,
    tracking_number,
    tracking_company,
    tracking_numbers,
    admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_fulfillments_hashid
from {{ ref('dev_and_meorders_fulfillments_ab3') }}
-- fulfillments at dev_and_meorders/fulfillments from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

