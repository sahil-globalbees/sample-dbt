{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    unique_key = '_airbyte_ab_id',
    schema = "public",
    tags = [ "top-level" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_mecustomers_ab3') }}
select
    {{ adapter.quote('id') }},
    note,
    tags,
    email,
    phone,
    {{ adapter.quote('state') }},
    currency,
    addresses,
    last_name,
    created_at,
    first_name,
    tax_exempt,
    updated_at,
    total_spent,
    orders_count,
    last_order_id,
    verified_email,
    default_address,
    last_order_name,
    accepts_marketing,
    admin_graphql_api_id,
    multipass_identifier,
    accepts_marketing_updated_at,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_dev_and_mecustomers_hashid
from {{ ref('dev_and_mecustomers_ab3') }}
-- dev_and_mecustomers from {{ source('public', '_airbyte_raw_dev_and_mecustomers') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

