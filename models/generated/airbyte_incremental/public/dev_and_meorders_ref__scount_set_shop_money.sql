{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref__scount_set_shop_money_ab3') }}
select
    _airbyte_total_discount_set_hashid,
    amount,
    currency_code,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_shop_money_hashid
from {{ ref('dev_and_meorders_ref__scount_set_shop_money_ab3') }}
-- shop_money at dev_and_meorders/refunds/refund_line_items/line_item/total_discount_set/shop_money from {{ ref('dev_and_meorders_ref__em_total_discount_set') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

