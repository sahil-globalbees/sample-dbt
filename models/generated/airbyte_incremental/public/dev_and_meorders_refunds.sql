{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_refunds_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ adapter.quote('id') }},
    note,
    restock,
    user_id,
    order_id,
    created_at,
    processed_at,
    transactions,
    order_adjustments,
    refund_line_items,
    admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_refunds_hashid
from {{ ref('dev_and_meorders_refunds_ab3') }}
-- refunds at dev_and_meorders/refunds from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

