{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref___transactions_receipt_ab3') }}
select
    _airbyte_transactions_hashid,
    paid_amount,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_receipt_hashid
from {{ ref('dev_and_meorders_ref___transactions_receipt_ab3') }}
-- receipt at dev_and_meorders/refunds/transactions/receipt from {{ ref('dev_and_meorders_refunds_transactions') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

