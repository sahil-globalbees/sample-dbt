{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_line_items_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ adapter.quote('id') }},
    sku,
    {{ adapter.quote('name') }},
    grams,
    price,
    title,
    duties,
    vendor,
    taxable,
    quantity,
    gift_card,
    price_set,
    tax_lines,
    product_id,
    properties,
    variant_id,
    pre_tax_price,
    variant_title,
    product_exists,
    total_discount,
    origin_location,
    requires_shipping,
    fulfillment_status,
    total_discount_set,
    fulfillment_service,
    admin_graphql_api_id,
    destination_location,
    discount_allocations,
    fulfillable_quantity,
    variant_inventory_management,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_line_items_hashid
from {{ ref('dev_and_meorders_line_items_ab3') }}
-- line_items at dev_and_meorders/line_items from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

