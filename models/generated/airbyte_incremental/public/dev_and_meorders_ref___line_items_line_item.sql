{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref___line_items_line_item_ab3') }}
select
    _airbyte_refund_line_items_hashid,
    {{ adapter.quote('id') }},
    sku,
    {{ adapter.quote('name') }},
    grams,
    price,
    title,
    vendor,
    taxable,
    quantity,
    gift_card,
    price_set,
    tax_lines,
    product_id,
    properties,
    variant_id,
    variant_title,
    product_exists,
    total_discount,
    requires_shipping,
    fulfillment_status,
    total_discount_set,
    fulfillment_service,
    admin_graphql_api_id,
    discount_allocations,
    fulfillable_quantity,
    variant_inventory_management,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_line_item_hashid
from {{ ref('dev_and_meorders_ref___line_items_line_item_ab3') }}
-- line_item at dev_and_meorders/refunds/refund_line_items/line_item from {{ ref('dev_and_meorders_refunds_refund_line_items') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

