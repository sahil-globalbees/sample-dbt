{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_client_details_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    browser_ip,
    user_agent,
    session_hash,
    browser_width,
    browser_height,
    accept_language,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_client_details_hashid
from {{ ref('dev_and_meorders_client_details_ab3') }}
-- client_details at dev_and_meorders/client_details from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

