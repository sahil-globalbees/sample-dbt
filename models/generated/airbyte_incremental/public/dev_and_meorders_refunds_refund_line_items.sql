{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_refunds_refund_line_items_ab3') }}
select
    _airbyte_refunds_hashid,
    {{ adapter.quote('id') }},
    quantity,
    subtotal,
    line_item,
    total_tax,
    location_id,
    line_item_id,
    restock_type,
    subtotal_set,
    total_tax_set,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_refund_line_items_hashid
from {{ ref('dev_and_meorders_refunds_refund_line_items_ab3') }}
-- refund_line_items at dev_and_meorders/refunds/refund_line_items from {{ ref('dev_and_meorders_refunds') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

