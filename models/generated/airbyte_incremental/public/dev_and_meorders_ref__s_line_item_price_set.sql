{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref__s_line_item_price_set_ab3') }}
select
    _airbyte_line_item_hashid,
    shop_money,
    presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_price_set_hashid
from {{ ref('dev_and_meorders_ref__s_line_item_price_set_ab3') }}
-- price_set at dev_and_meorders/refunds/refund_line_items/line_item/price_set from {{ ref('dev_and_meorders_ref___line_items_line_item') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

