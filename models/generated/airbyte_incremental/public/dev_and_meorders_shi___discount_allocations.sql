{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_shi___discount_allocations_ab3') }}
select
    _airbyte_shipping_lines_hashid,
    {{ adapter.quote('id') }},
    amount,
    amount_set,
    created_at,
    description,
    application_type,
    discount_application_index,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_discount_allocations_hashid
from {{ ref('dev_and_meorders_shi___discount_allocations_ab3') }}
-- discount_allocations at dev_and_meorders/shipping_lines/discount_allocations from {{ ref('dev_and_meorders_shipping_lines') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

