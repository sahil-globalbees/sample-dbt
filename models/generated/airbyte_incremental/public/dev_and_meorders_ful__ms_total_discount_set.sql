{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ful__ms_total_discount_set_ab3') }}
select
    _airbyte_line_items_hashid,
    shop_money,
    presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_total_discount_set_hashid
from {{ ref('dev_and_meorders_ful__ms_total_discount_set_ab3') }}
-- total_discount_set at dev_and_meorders/fulfillments/line_items/total_discount_set from {{ ref('dev_and_meorders_fulfillments_line_items') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

