{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_payment_details_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    avs_result_code,
    credit_card_bin,
    cvv_result_code,
    credit_card_number,
    credit_card_company,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_payment_details_hashid
from {{ ref('dev_and_meorders_payment_details_ab3') }}
-- payment_details at dev_and_meorders/payment_details from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

