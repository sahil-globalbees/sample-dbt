{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    unique_key = '_airbyte_ab_id',
    schema = "public",
    tags = [ "top-level" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meproducts_ab3') }}
select
    {{ adapter.quote('id') }},
    tags,
    image,
    title,
    handle,
    images,
    status,
    vendor,
    {{ adapter.quote('options') }},
    variants,
    body_html,
    created_at,
    updated_at,
    product_type,
    published_at,
    published_scope,
    template_suffix,
    admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_dev_and_meproducts_hashid
from {{ ref('dev_and_meproducts_ab3') }}
-- dev_and_meproducts from {{ source('public', '_airbyte_raw_dev_and_meproducts') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

