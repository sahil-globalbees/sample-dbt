{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_refunds_order_adjustments_ab3') }}
select
    _airbyte_refunds_hashid,
    {{ adapter.quote('id') }},
    kind,
    amount,
    reason,
    order_id,
    refund_id,
    amount_set,
    tax_amount,
    tax_amount_set,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_order_adjustments_hashid
from {{ ref('dev_and_meorders_refunds_order_adjustments_ab3') }}
-- order_adjustments at dev_and_meorders/refunds/order_adjustments from {{ ref('dev_and_meorders_refunds') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

