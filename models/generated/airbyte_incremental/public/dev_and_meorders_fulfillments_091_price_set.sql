{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_fulfillments_091_price_set_ab3') }}
select
    _airbyte_tax_lines_hashid,
    shop_money,
    presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_price_set_hashid
from {{ ref('dev_and_meorders_fulfillments_091_price_set_ab3') }}
-- price_set at dev_and_meorders/fulfillments/line_items/duties/tax_lines/price_set from {{ ref('dev_and_meorders_ful__tems_duties_tax_lines') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

