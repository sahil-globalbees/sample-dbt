{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_shipping_lines_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ adapter.quote('id') }},
    code,
    phone,
    price,
    title,
    {{ adapter.quote('source') }},
    price_set,
    tax_lines,
    discounted_price,
    delivery_category,
    carrier_identifier,
    discount_allocations,
    discounted_price_set,
    requested_fulfillment_service_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_shipping_lines_hashid
from {{ ref('dev_and_meorders_shipping_lines_ab3') }}
-- shipping_lines at dev_and_meorders/shipping_lines from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

