{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref__e_items_total_tax_set_ab3') }}
select
    _airbyte_refund_line_items_hashid,
    shop_money,
    presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_total_tax_set_hashid
from {{ ref('dev_and_meorders_ref__e_items_total_tax_set_ab3') }}
-- total_tax_set at dev_and_meorders/refunds/refund_line_items/total_tax_set from {{ ref('dev_and_meorders_refunds_refund_line_items') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

