{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_line_ab4_presentment_money_ab3') }}
select
    _airbyte_price_set_hashid,
    amount,
    currency_code,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_presentment_money_hashid
from {{ ref('dev_and_meorders_line_ab4_presentment_money_ab3') }}
-- presentment_money at dev_and_meorders/line_items/price_set/presentment_money from {{ ref('dev_and_meorders_line_items_price_set') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

