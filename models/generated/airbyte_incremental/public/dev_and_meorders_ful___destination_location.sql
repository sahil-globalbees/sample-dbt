{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ful___destination_location_ab3') }}
select
    _airbyte_line_items_hashid,
    {{ adapter.quote('id') }},
    zip,
    city,
    {{ adapter.quote('name') }},
    address1,
    address2,
    country_code,
    province_code,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_destination_location_hashid
from {{ ref('dev_and_meorders_ful___destination_location_ab3') }}
-- destination_location at dev_and_meorders/fulfillments/line_items/destination_location from {{ ref('dev_and_meorders_fulfillments_line_items') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

