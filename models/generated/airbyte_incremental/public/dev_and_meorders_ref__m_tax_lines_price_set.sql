{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_ref__m_tax_lines_price_set_ab3') }}
select
    _airbyte_tax_lines_hashid,
    shop_money,
    presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_price_set_hashid
from {{ ref('dev_and_meorders_ref__m_tax_lines_price_set_ab3') }}
-- price_set at dev_and_meorders/refunds/refund_line_items/line_item/tax_lines/price_set from {{ ref('dev_and_meorders_ref__s_line_item_tax_lines') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

