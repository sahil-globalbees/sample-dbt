{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_refunds_transactions_ab3') }}
select
    _airbyte_refunds_hashid,
    {{ adapter.quote('id') }},
    kind,
    test,
    amount,
    status,
    gateway,
    message,
    receipt,
    user_id,
    currency,
    order_id,
    device_id,
    parent_id,
    created_at,
    error_code,
    location_id,
    source_name,
    processed_at,
    {{ adapter.quote('authorization') }},
    admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_transactions_hashid
from {{ ref('dev_and_meorders_refunds_transactions_ab3') }}
-- transactions at dev_and_meorders/refunds/transactions from {{ ref('dev_and_meorders_refunds') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

