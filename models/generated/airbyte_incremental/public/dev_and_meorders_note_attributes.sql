{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "public",
    tags = [ "nested" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('dev_and_meorders_note_attributes_ab3') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ adapter.quote('name') }},
    {{ adapter.quote('value') }},
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at,
    _airbyte_note_attributes_hashid
from {{ ref('dev_and_meorders_note_attributes_ab3') }}
-- note_attributes at dev_and_meorders/note_attributes from {{ ref('dev_and_meorders') }}
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

