{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_refunds_refund_line_items_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_refunds_hashid',
        adapter.quote('id'),
        'quantity',
        'subtotal',
        'line_item',
        'total_tax',
        'location_id',
        'line_item_id',
        'restock_type',
        'subtotal_set',
        'total_tax_set',
    ]) }} as _airbyte_refund_line_items_hashid,
    tmp.*
from {{ ref('dev_and_meorders_refunds_refund_line_items_ab2') }} tmp
-- refund_line_items at dev_and_meorders/refunds/refund_line_items
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

