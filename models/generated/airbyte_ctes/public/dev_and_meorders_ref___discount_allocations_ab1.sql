{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_ref___line_items_line_item') }}
{{ unnest_cte(ref('dev_and_meorders_ref___line_items_line_item'), 'line_item', 'discount_allocations') }}
select
    _airbyte_line_item_hashid,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['amount'], ['amount']) }} as amount,
    {{ json_extract('', unnested_column_value('discount_allocations'), ['amount_set'], ['amount_set']) }} as amount_set,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['discount_application_index'], ['discount_application_index']) }} as discount_application_index,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_ref___line_items_line_item') }} as table_alias
-- discount_allocations at dev_and_meorders/refunds/refund_line_items/line_item/discount_allocations
{{ cross_join_unnest('line_item', 'discount_allocations') }}
where 1 = 1
and discount_allocations is not null
{{ incremental_clause('_airbyte_emitted_at') }}

