{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_refunds') }}
{{ unnest_cte(ref('dev_and_meorders_refunds'), 'refunds', 'transactions') }}
select
    _airbyte_refunds_hashid,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('transactions'), ['kind'], ['kind']) }} as kind,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['test'], ['test']) }} as test,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['amount'], ['amount']) }} as amount,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['status'], ['status']) }} as status,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['gateway'], ['gateway']) }} as gateway,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['message'], ['message']) }} as message,
    {{ json_extract('', unnested_column_value('transactions'), ['receipt'], ['receipt']) }} as receipt,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['user_id'], ['user_id']) }} as user_id,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['currency'], ['currency']) }} as currency,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['order_id'], ['order_id']) }} as order_id,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['device_id'], ['device_id']) }} as device_id,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['parent_id'], ['parent_id']) }} as parent_id,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['created_at'], ['created_at']) }} as created_at,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['error_code'], ['error_code']) }} as error_code,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['location_id'], ['location_id']) }} as location_id,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['source_name'], ['source_name']) }} as source_name,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['processed_at'], ['processed_at']) }} as processed_at,
    {{ json_extract_scalar(unnested_column_value('transactions'), ['authorization'], ['authorization']) }} as {{ adapter.quote('authorization') }},
    {{ json_extract_scalar(unnested_column_value('transactions'), ['admin_graphql_api_id'], ['admin_graphql_api_id']) }} as admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds') }} as table_alias
-- transactions at dev_and_meorders/refunds/transactions
{{ cross_join_unnest('refunds', 'transactions') }}
where 1 = 1
and transactions is not null
{{ incremental_clause('_airbyte_emitted_at') }}

