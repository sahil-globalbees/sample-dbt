{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_cur__t_total_discounts_set_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        'shop_money',
        'presentment_money',
    ]) }} as _airbyte_current_total_discounts_set_hashid,
    tmp.*
from {{ ref('dev_and_meorders_cur__t_total_discounts_set_ab2') }} tmp
-- current_total_discounts_set at dev_and_meorders/current_total_discounts_set
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

