{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_refunds_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        adapter.quote('id'),
        'note',
        boolean_to_string('restock'),
        'user_id',
        'order_id',
        'created_at',
        'processed_at',
        array_to_string('transactions'),
        array_to_string('order_adjustments'),
        array_to_string('refund_line_items'),
        'admin_graphql_api_id',
    ]) }} as _airbyte_refunds_hashid,
    tmp.*
from {{ ref('dev_and_meorders_refunds_ab2') }} tmp
-- refunds at dev_and_meorders/refunds
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

