{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_fulfillments_line_items_ab1') }}
select
    _airbyte_fulfillments_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast(sku as {{ dbt_utils.type_string() }}) as sku,
    cast({{ adapter.quote('name') }} as {{ dbt_utils.type_string() }}) as {{ adapter.quote('name') }},
    cast(grams as {{ dbt_utils.type_bigint() }}) as grams,
    cast(price as {{ dbt_utils.type_float() }}) as price,
    cast(title as {{ dbt_utils.type_string() }}) as title,
    duties,
    cast(vendor as {{ dbt_utils.type_string() }}) as vendor,
    {{ cast_to_boolean('taxable') }} as taxable,
    cast(quantity as {{ dbt_utils.type_bigint() }}) as quantity,
    {{ cast_to_boolean('gift_card') }} as gift_card,
    cast(price_set as {{ type_json() }}) as price_set,
    tax_lines,
    cast(product_id as {{ dbt_utils.type_bigint() }}) as product_id,
    properties,
    cast(variant_id as {{ dbt_utils.type_bigint() }}) as variant_id,
    cast(pre_tax_price as {{ dbt_utils.type_float() }}) as pre_tax_price,
    cast(variant_title as {{ dbt_utils.type_string() }}) as variant_title,
    {{ cast_to_boolean('product_exists') }} as product_exists,
    cast(total_discount as {{ dbt_utils.type_float() }}) as total_discount,
    cast(origin_location as {{ type_json() }}) as origin_location,
    {{ cast_to_boolean('requires_shipping') }} as requires_shipping,
    cast(fulfillment_status as {{ dbt_utils.type_string() }}) as fulfillment_status,
    cast(total_discount_set as {{ type_json() }}) as total_discount_set,
    cast(fulfillment_service as {{ dbt_utils.type_string() }}) as fulfillment_service,
    cast(admin_graphql_api_id as {{ dbt_utils.type_string() }}) as admin_graphql_api_id,
    cast(destination_location as {{ type_json() }}) as destination_location,
    discount_allocations,
    cast(fulfillable_quantity as {{ dbt_utils.type_bigint() }}) as fulfillable_quantity,
    cast(variant_inventory_management as {{ dbt_utils.type_string() }}) as variant_inventory_management,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_fulfillments_line_items_ab1') }}
-- line_items at dev_and_meorders/fulfillments/line_items
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

