{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar('payment_details', ['avs_result_code'], ['avs_result_code']) }} as avs_result_code,
    {{ json_extract_scalar('payment_details', ['credit_card_bin'], ['credit_card_bin']) }} as credit_card_bin,
    {{ json_extract_scalar('payment_details', ['cvv_result_code'], ['cvv_result_code']) }} as cvv_result_code,
    {{ json_extract_scalar('payment_details', ['credit_card_number'], ['credit_card_number']) }} as credit_card_number,
    {{ json_extract_scalar('payment_details', ['credit_card_company'], ['credit_card_company']) }} as credit_card_company,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- payment_details at dev_and_meorders/payment_details
where 1 = 1
and payment_details is not null
{{ incremental_clause('_airbyte_emitted_at') }}

