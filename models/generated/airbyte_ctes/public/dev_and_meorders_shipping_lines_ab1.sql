{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
{{ unnest_cte(ref('dev_and_meorders'), 'dev_and_meorders', 'shipping_lines') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['code'], ['code']) }} as code,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['phone'], ['phone']) }} as phone,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['price'], ['price']) }} as price,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['title'], ['title']) }} as title,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['source'], ['source']) }} as {{ adapter.quote('source') }},
    {{ json_extract('', unnested_column_value('shipping_lines'), ['price_set'], ['price_set']) }} as price_set,
    {{ json_extract_array(unnested_column_value('shipping_lines'), ['tax_lines'], ['tax_lines']) }} as tax_lines,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['discounted_price'], ['discounted_price']) }} as discounted_price,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['delivery_category'], ['delivery_category']) }} as delivery_category,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['carrier_identifier'], ['carrier_identifier']) }} as carrier_identifier,
    {{ json_extract_array(unnested_column_value('shipping_lines'), ['discount_allocations'], ['discount_allocations']) }} as discount_allocations,
    {{ json_extract('', unnested_column_value('shipping_lines'), ['discounted_price_set'], ['discounted_price_set']) }} as discounted_price_set,
    {{ json_extract_scalar(unnested_column_value('shipping_lines'), ['requested_fulfillment_service_id'], ['requested_fulfillment_service_id']) }} as requested_fulfillment_service_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- shipping_lines at dev_and_meorders/shipping_lines
{{ cross_join_unnest('dev_and_meorders', 'shipping_lines') }}
where 1 = 1
and shipping_lines is not null
{{ incremental_clause('_airbyte_emitted_at') }}

