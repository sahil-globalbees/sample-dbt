{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_ful__nts_line_items_duties_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_line_items_hashid',
        adapter.quote('id'),
        array_to_string('tax_lines'),
        'shop_money',
        'presentment_money',
        'admin_graphql_api_id',
        'country_code_of_origin',
        'harmonized_system_code',
    ]) }} as _airbyte_duties_hashid,
    tmp.*
from {{ ref('dev_and_meorders_ful__nts_line_items_duties_ab2') }} tmp
-- duties at dev_and_meorders/fulfillments/line_items/duties
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

