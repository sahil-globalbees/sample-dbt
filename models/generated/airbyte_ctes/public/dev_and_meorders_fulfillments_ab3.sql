{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_fulfillments_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        adapter.quote('id'),
        adapter.quote('name'),
        'status',
        'receipt',
        'service',
        'order_id',
        'created_at',
        array_to_string('line_items'),
        'updated_at',
        'location_id',
        'tracking_url',
        array_to_string('tracking_urls'),
        'shipment_status',
        'tracking_number',
        'tracking_company',
        array_to_string('tracking_numbers'),
        'admin_graphql_api_id',
    ]) }} as _airbyte_fulfillments_hashid,
    tmp.*
from {{ ref('dev_and_meorders_fulfillments_ab2') }} tmp
-- fulfillments at dev_and_meorders/fulfillments
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

