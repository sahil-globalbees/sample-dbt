{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_refunds_order_adjustments_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_refunds_hashid',
        adapter.quote('id'),
        'kind',
        'amount',
        'reason',
        'order_id',
        'refund_id',
        'amount_set',
        'tax_amount',
        'tax_amount_set',
    ]) }} as _airbyte_order_adjustments_hashid,
    tmp.*
from {{ ref('dev_and_meorders_refunds_order_adjustments_ab2') }} tmp
-- order_adjustments at dev_and_meorders/refunds/order_adjustments
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

