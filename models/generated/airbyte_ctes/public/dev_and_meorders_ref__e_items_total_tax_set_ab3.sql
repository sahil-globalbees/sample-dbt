{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_ref__e_items_total_tax_set_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_refund_line_items_hashid',
        'shop_money',
        'presentment_money',
    ]) }} as _airbyte_total_tax_set_hashid,
    tmp.*
from {{ ref('dev_and_meorders_ref__e_items_total_tax_set_ab2') }} tmp
-- total_tax_set at dev_and_meorders/refunds/refund_line_items/total_tax_set
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

