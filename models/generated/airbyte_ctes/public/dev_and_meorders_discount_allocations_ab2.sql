{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_discount_allocations_ab1') }}
select
    _airbyte_dev_and_meorders_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_string() }}) as {{ adapter.quote('id') }},
    cast(amount as {{ dbt_utils.type_string() }}) as amount,
    cast(amount_set as {{ type_json() }}) as amount_set,
    cast({{ empty_string_to_null('created_at') }} as {{ type_timestamp_with_timezone() }}) as created_at,
    cast(description as {{ dbt_utils.type_string() }}) as description,
    cast(application_type as {{ dbt_utils.type_string() }}) as application_type,
    cast(discount_application_index as {{ dbt_utils.type_float() }}) as discount_application_index,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_discount_allocations_ab1') }}
-- discount_allocations at dev_and_meorders/discount_allocations
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

