{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_refunds_refund_line_items') }}
select
    _airbyte_refund_line_items_hashid,
    {{ json_extract('table_alias', 'subtotal_set', ['shop_money'], ['shop_money']) }} as shop_money,
    {{ json_extract('table_alias', 'subtotal_set', ['presentment_money'], ['presentment_money']) }} as presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_refund_line_items') }} as table_alias
-- subtotal_set at dev_and_meorders/refunds/refund_line_items/subtotal_set
where 1 = 1
and subtotal_set is not null
{{ incremental_clause('_airbyte_emitted_at') }}

