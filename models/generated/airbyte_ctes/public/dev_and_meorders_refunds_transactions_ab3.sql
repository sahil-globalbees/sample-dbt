{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_refunds_transactions_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_refunds_hashid',
        adapter.quote('id'),
        'kind',
        boolean_to_string('test'),
        'amount',
        'status',
        'gateway',
        'message',
        'receipt',
        'user_id',
        'currency',
        'order_id',
        'device_id',
        'parent_id',
        'created_at',
        'error_code',
        'location_id',
        'source_name',
        'processed_at',
        adapter.quote('authorization'),
        'admin_graphql_api_id',
    ]) }} as _airbyte_transactions_hashid,
    tmp.*
from {{ ref('dev_and_meorders_refunds_transactions_ab2') }} tmp
-- transactions at dev_and_meorders/refunds/transactions
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

