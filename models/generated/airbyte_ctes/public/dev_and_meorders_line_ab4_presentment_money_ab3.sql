{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_line_ab4_presentment_money_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_price_set_hashid',
        'amount',
        'currency_code',
    ]) }} as _airbyte_presentment_money_hashid,
    tmp.*
from {{ ref('dev_and_meorders_line_ab4_presentment_money_ab2') }} tmp
-- presentment_money at dev_and_meorders/line_items/price_set/presentment_money
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

