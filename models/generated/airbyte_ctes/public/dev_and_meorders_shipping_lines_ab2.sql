{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_shipping_lines_ab1') }}
select
    _airbyte_dev_and_meorders_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast(code as {{ dbt_utils.type_string() }}) as code,
    cast(phone as {{ dbt_utils.type_string() }}) as phone,
    cast(price as {{ dbt_utils.type_float() }}) as price,
    cast(title as {{ dbt_utils.type_string() }}) as title,
    cast({{ adapter.quote('source') }} as {{ dbt_utils.type_string() }}) as {{ adapter.quote('source') }},
    cast(price_set as {{ type_json() }}) as price_set,
    tax_lines,
    cast(discounted_price as {{ dbt_utils.type_float() }}) as discounted_price,
    cast(delivery_category as {{ dbt_utils.type_string() }}) as delivery_category,
    cast(carrier_identifier as {{ dbt_utils.type_string() }}) as carrier_identifier,
    discount_allocations,
    cast(discounted_price_set as {{ type_json() }}) as discounted_price_set,
    cast(requested_fulfillment_service_id as {{ dbt_utils.type_string() }}) as requested_fulfillment_service_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_shipping_lines_ab1') }}
-- shipping_lines at dev_and_meorders/shipping_lines
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

