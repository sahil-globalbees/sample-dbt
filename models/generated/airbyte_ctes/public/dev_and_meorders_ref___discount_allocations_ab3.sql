{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_ref___discount_allocations_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_line_item_hashid',
        'amount',
        'amount_set',
        'discount_application_index',
    ]) }} as _airbyte_discount_allocations_hashid,
    tmp.*
from {{ ref('dev_and_meorders_ref___discount_allocations_ab2') }} tmp
-- discount_allocations at dev_and_meorders/refunds/refund_line_items/line_item/discount_allocations
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

