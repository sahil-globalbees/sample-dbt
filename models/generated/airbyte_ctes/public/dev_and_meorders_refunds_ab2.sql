{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_refunds_ab1') }}
select
    _airbyte_dev_and_meorders_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast(note as {{ dbt_utils.type_string() }}) as note,
    {{ cast_to_boolean('restock') }} as restock,
    cast(user_id as {{ dbt_utils.type_bigint() }}) as user_id,
    cast(order_id as {{ dbt_utils.type_bigint() }}) as order_id,
    cast({{ empty_string_to_null('created_at') }} as {{ type_timestamp_with_timezone() }}) as created_at,
    cast({{ empty_string_to_null('processed_at') }} as {{ type_timestamp_with_timezone() }}) as processed_at,
    transactions,
    order_adjustments,
    refund_line_items,
    cast(admin_graphql_api_id as {{ dbt_utils.type_string() }}) as admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_ab1') }}
-- refunds at dev_and_meorders/refunds
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

