{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_tax_lines_price_set_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_tax_lines_hashid',
        'shop_money',
        'presentment_money',
    ]) }} as _airbyte_price_set_hashid,
    tmp.*
from {{ ref('dev_and_meorders_tax_lines_price_set_ab2') }} tmp
-- price_set at dev_and_meorders/tax_lines/price_set
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

