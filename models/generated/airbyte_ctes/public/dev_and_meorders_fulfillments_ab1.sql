{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
{{ unnest_cte(ref('dev_and_meorders'), 'dev_and_meorders', 'fulfillments') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['name'], ['name']) }} as {{ adapter.quote('name') }},
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['status'], ['status']) }} as status,
    {{ json_extract('', unnested_column_value('fulfillments'), ['receipt'], ['receipt']) }} as receipt,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['service'], ['service']) }} as service,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['order_id'], ['order_id']) }} as order_id,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['created_at'], ['created_at']) }} as created_at,
    {{ json_extract_array(unnested_column_value('fulfillments'), ['line_items'], ['line_items']) }} as line_items,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['updated_at'], ['updated_at']) }} as updated_at,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['location_id'], ['location_id']) }} as location_id,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['tracking_url'], ['tracking_url']) }} as tracking_url,
    {{ json_extract_array(unnested_column_value('fulfillments'), ['tracking_urls'], ['tracking_urls']) }} as tracking_urls,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['shipment_status'], ['shipment_status']) }} as shipment_status,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['tracking_number'], ['tracking_number']) }} as tracking_number,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['tracking_company'], ['tracking_company']) }} as tracking_company,
    {{ json_extract_array(unnested_column_value('fulfillments'), ['tracking_numbers'], ['tracking_numbers']) }} as tracking_numbers,
    {{ json_extract_scalar(unnested_column_value('fulfillments'), ['admin_graphql_api_id'], ['admin_graphql_api_id']) }} as admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- fulfillments at dev_and_meorders/fulfillments
{{ cross_join_unnest('dev_and_meorders', 'fulfillments') }}
where 1 = 1
and fulfillments is not null
{{ incremental_clause('_airbyte_emitted_at') }}

