{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_lin__tems_duties_tax_lines_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_duties_hashid',
        'rate',
        'price',
        'title',
        'price_set',
        boolean_to_string('channel_liable'),
    ]) }} as _airbyte_tax_lines_hashid,
    tmp.*
from {{ ref('dev_and_meorders_lin__tems_duties_tax_lines_ab2') }} tmp
-- tax_lines at dev_and_meorders/line_items/duties/tax_lines
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

