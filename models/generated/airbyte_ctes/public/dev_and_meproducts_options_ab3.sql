{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meproducts_options_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meproducts_hashid',
        adapter.quote('id'),
        adapter.quote('name'),
        array_to_string(adapter.quote('values')),
        adapter.quote('position'),
        'product_id',
    ]) }} as _airbyte_options_hashid,
    tmp.*
from {{ ref('dev_and_meproducts_options_ab2') }} tmp
-- options at dev_and_meproducts/options
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

