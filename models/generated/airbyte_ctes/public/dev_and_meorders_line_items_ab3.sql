{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_line_items_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        adapter.quote('id'),
        'sku',
        adapter.quote('name'),
        'grams',
        'price',
        'title',
        array_to_string('duties'),
        'vendor',
        boolean_to_string('taxable'),
        'quantity',
        boolean_to_string('gift_card'),
        'price_set',
        array_to_string('tax_lines'),
        'product_id',
        array_to_string('properties'),
        'variant_id',
        'pre_tax_price',
        'variant_title',
        boolean_to_string('product_exists'),
        'total_discount',
        'origin_location',
        boolean_to_string('requires_shipping'),
        'fulfillment_status',
        'total_discount_set',
        'fulfillment_service',
        'admin_graphql_api_id',
        'destination_location',
        array_to_string('discount_allocations'),
        'fulfillable_quantity',
        'variant_inventory_management',
    ]) }} as _airbyte_line_items_hashid,
    tmp.*
from {{ ref('dev_and_meorders_line_items_ab2') }} tmp
-- line_items at dev_and_meorders/line_items
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

