{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_payment_details_ab1') }}
select
    _airbyte_dev_and_meorders_hashid,
    cast(avs_result_code as {{ dbt_utils.type_string() }}) as avs_result_code,
    cast(credit_card_bin as {{ dbt_utils.type_string() }}) as credit_card_bin,
    cast(cvv_result_code as {{ dbt_utils.type_string() }}) as cvv_result_code,
    cast(credit_card_number as {{ dbt_utils.type_string() }}) as credit_card_number,
    cast(credit_card_company as {{ dbt_utils.type_string() }}) as credit_card_company,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_payment_details_ab1') }}
-- payment_details at dev_and_meorders/payment_details
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

