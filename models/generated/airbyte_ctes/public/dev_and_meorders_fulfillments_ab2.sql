{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_fulfillments_ab1') }}
select
    _airbyte_dev_and_meorders_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast({{ adapter.quote('name') }} as {{ dbt_utils.type_string() }}) as {{ adapter.quote('name') }},
    cast(status as {{ dbt_utils.type_string() }}) as status,
    cast(receipt as {{ type_json() }}) as receipt,
    cast(service as {{ dbt_utils.type_string() }}) as service,
    cast(order_id as {{ dbt_utils.type_bigint() }}) as order_id,
    cast({{ empty_string_to_null('created_at') }} as {{ type_timestamp_with_timezone() }}) as created_at,
    line_items,
    cast({{ empty_string_to_null('updated_at') }} as {{ type_timestamp_with_timezone() }}) as updated_at,
    cast(location_id as {{ dbt_utils.type_bigint() }}) as location_id,
    cast(tracking_url as {{ dbt_utils.type_string() }}) as tracking_url,
    tracking_urls,
    cast(shipment_status as {{ dbt_utils.type_string() }}) as shipment_status,
    cast(tracking_number as {{ dbt_utils.type_string() }}) as tracking_number,
    cast(tracking_company as {{ dbt_utils.type_string() }}) as tracking_company,
    tracking_numbers,
    cast(admin_graphql_api_id as {{ dbt_utils.type_string() }}) as admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_fulfillments_ab1') }}
-- fulfillments at dev_and_meorders/fulfillments
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

