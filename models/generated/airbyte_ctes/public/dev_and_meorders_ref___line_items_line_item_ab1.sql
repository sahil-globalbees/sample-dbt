{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_refunds_refund_line_items') }}
select
    _airbyte_refund_line_items_hashid,
    {{ json_extract_scalar('line_item', ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar('line_item', ['sku'], ['sku']) }} as sku,
    {{ json_extract_scalar('line_item', ['name'], ['name']) }} as {{ adapter.quote('name') }},
    {{ json_extract_scalar('line_item', ['grams'], ['grams']) }} as grams,
    {{ json_extract_scalar('line_item', ['price'], ['price']) }} as price,
    {{ json_extract_scalar('line_item', ['title'], ['title']) }} as title,
    {{ json_extract_scalar('line_item', ['vendor'], ['vendor']) }} as vendor,
    {{ json_extract_scalar('line_item', ['taxable'], ['taxable']) }} as taxable,
    {{ json_extract_scalar('line_item', ['quantity'], ['quantity']) }} as quantity,
    {{ json_extract_scalar('line_item', ['gift_card'], ['gift_card']) }} as gift_card,
    {{ json_extract('table_alias', 'line_item', ['price_set'], ['price_set']) }} as price_set,
    {{ json_extract_array('line_item', ['tax_lines'], ['tax_lines']) }} as tax_lines,
    {{ json_extract_scalar('line_item', ['product_id'], ['product_id']) }} as product_id,
    {{ json_extract_array('line_item', ['properties'], ['properties']) }} as properties,
    {{ json_extract_scalar('line_item', ['variant_id'], ['variant_id']) }} as variant_id,
    {{ json_extract_scalar('line_item', ['variant_title'], ['variant_title']) }} as variant_title,
    {{ json_extract_scalar('line_item', ['product_exists'], ['product_exists']) }} as product_exists,
    {{ json_extract_scalar('line_item', ['total_discount'], ['total_discount']) }} as total_discount,
    {{ json_extract_scalar('line_item', ['requires_shipping'], ['requires_shipping']) }} as requires_shipping,
    {{ json_extract_scalar('line_item', ['fulfillment_status'], ['fulfillment_status']) }} as fulfillment_status,
    {{ json_extract('table_alias', 'line_item', ['total_discount_set'], ['total_discount_set']) }} as total_discount_set,
    {{ json_extract_scalar('line_item', ['fulfillment_service'], ['fulfillment_service']) }} as fulfillment_service,
    {{ json_extract_scalar('line_item', ['admin_graphql_api_id'], ['admin_graphql_api_id']) }} as admin_graphql_api_id,
    {{ json_extract_array('line_item', ['discount_allocations'], ['discount_allocations']) }} as discount_allocations,
    {{ json_extract_scalar('line_item', ['fulfillable_quantity'], ['fulfillable_quantity']) }} as fulfillable_quantity,
    {{ json_extract_scalar('line_item', ['variant_inventory_management'], ['variant_inventory_management']) }} as variant_inventory_management,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_refund_line_items') }} as table_alias
-- line_item at dev_and_meorders/refunds/refund_line_items/line_item
where 1 = 1
and line_item is not null
{{ incremental_clause('_airbyte_emitted_at') }}

