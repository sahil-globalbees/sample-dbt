{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_fulfillments_line_items') }}
{{ unnest_cte(ref('dev_and_meorders_fulfillments_line_items'), 'line_items', 'duties') }}
select
    _airbyte_line_items_hashid,
    {{ json_extract_scalar(unnested_column_value('duties'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_array(unnested_column_value('duties'), ['tax_lines'], ['tax_lines']) }} as tax_lines,
    {{ json_extract('', unnested_column_value('duties'), ['shop_money'], ['shop_money']) }} as shop_money,
    {{ json_extract('', unnested_column_value('duties'), ['presentment_money'], ['presentment_money']) }} as presentment_money,
    {{ json_extract_scalar(unnested_column_value('duties'), ['admin_graphql_api_id'], ['admin_graphql_api_id']) }} as admin_graphql_api_id,
    {{ json_extract_scalar(unnested_column_value('duties'), ['country_code_of_origin'], ['country_code_of_origin']) }} as country_code_of_origin,
    {{ json_extract_scalar(unnested_column_value('duties'), ['harmonized_system_code'], ['harmonized_system_code']) }} as harmonized_system_code,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_fulfillments_line_items') }} as table_alias
-- duties at dev_and_meorders/fulfillments/line_items/duties
{{ cross_join_unnest('line_items', 'duties') }}
where 1 = 1
and duties is not null
{{ incremental_clause('_airbyte_emitted_at') }}

