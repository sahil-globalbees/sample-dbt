{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_fulfillments_line_items') }}
select
    _airbyte_line_items_hashid,
    {{ json_extract_scalar('origin_location', ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar('origin_location', ['zip'], ['zip']) }} as zip,
    {{ json_extract_scalar('origin_location', ['city'], ['city']) }} as city,
    {{ json_extract_scalar('origin_location', ['name'], ['name']) }} as {{ adapter.quote('name') }},
    {{ json_extract_scalar('origin_location', ['address1'], ['address1']) }} as address1,
    {{ json_extract_scalar('origin_location', ['address2'], ['address2']) }} as address2,
    {{ json_extract_scalar('origin_location', ['country_code'], ['country_code']) }} as country_code,
    {{ json_extract_scalar('origin_location', ['province_code'], ['province_code']) }} as province_code,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_fulfillments_line_items') }} as table_alias
-- origin_location at dev_and_meorders/fulfillments/line_items/origin_location
where 1 = 1
and origin_location is not null
{{ incremental_clause('_airbyte_emitted_at') }}

