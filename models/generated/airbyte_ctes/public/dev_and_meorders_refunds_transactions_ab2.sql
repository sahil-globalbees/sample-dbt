{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_refunds_transactions_ab1') }}
select
    _airbyte_refunds_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast(kind as {{ dbt_utils.type_string() }}) as kind,
    {{ cast_to_boolean('test') }} as test,
    cast(amount as {{ dbt_utils.type_string() }}) as amount,
    cast(status as {{ dbt_utils.type_string() }}) as status,
    cast(gateway as {{ dbt_utils.type_string() }}) as gateway,
    cast(message as {{ dbt_utils.type_string() }}) as message,
    cast(receipt as {{ type_json() }}) as receipt,
    cast(user_id as {{ dbt_utils.type_bigint() }}) as user_id,
    cast(currency as {{ dbt_utils.type_string() }}) as currency,
    cast(order_id as {{ dbt_utils.type_bigint() }}) as order_id,
    cast(device_id as {{ dbt_utils.type_bigint() }}) as device_id,
    cast(parent_id as {{ dbt_utils.type_bigint() }}) as parent_id,
    cast(created_at as {{ dbt_utils.type_string() }}) as created_at,
    cast(error_code as {{ dbt_utils.type_string() }}) as error_code,
    cast(location_id as {{ dbt_utils.type_bigint() }}) as location_id,
    cast(source_name as {{ dbt_utils.type_string() }}) as source_name,
    cast(processed_at as {{ dbt_utils.type_string() }}) as processed_at,
    cast({{ adapter.quote('authorization') }} as {{ dbt_utils.type_string() }}) as {{ adapter.quote('authorization') }},
    cast(admin_graphql_api_id as {{ dbt_utils.type_string() }}) as admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_transactions_ab1') }}
-- transactions at dev_and_meorders/refunds/transactions
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

