{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_payment_details_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        'avs_result_code',
        'credit_card_bin',
        'cvv_result_code',
        'credit_card_number',
        'credit_card_company',
    ]) }} as _airbyte_payment_details_hashid,
    tmp.*
from {{ ref('dev_and_meorders_payment_details_ab2') }} tmp
-- payment_details at dev_and_meorders/payment_details
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

