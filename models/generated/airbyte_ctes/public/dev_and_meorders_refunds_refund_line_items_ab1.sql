{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_refunds') }}
{{ unnest_cte(ref('dev_and_meorders_refunds'), 'refunds', 'refund_line_items') }}
select
    _airbyte_refunds_hashid,
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['quantity'], ['quantity']) }} as quantity,
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['subtotal'], ['subtotal']) }} as subtotal,
    {{ json_extract('', unnested_column_value('refund_line_items'), ['line_item'], ['line_item']) }} as line_item,
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['total_tax'], ['total_tax']) }} as total_tax,
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['location_id'], ['location_id']) }} as location_id,
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['line_item_id'], ['line_item_id']) }} as line_item_id,
    {{ json_extract_scalar(unnested_column_value('refund_line_items'), ['restock_type'], ['restock_type']) }} as restock_type,
    {{ json_extract('', unnested_column_value('refund_line_items'), ['subtotal_set'], ['subtotal_set']) }} as subtotal_set,
    {{ json_extract('', unnested_column_value('refund_line_items'), ['total_tax_set'], ['total_tax_set']) }} as total_tax_set,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds') }} as table_alias
-- refund_line_items at dev_and_meorders/refunds/refund_line_items
{{ cross_join_unnest('refunds', 'refund_line_items') }}
where 1 = 1
and refund_line_items is not null
{{ incremental_clause('_airbyte_emitted_at') }}

