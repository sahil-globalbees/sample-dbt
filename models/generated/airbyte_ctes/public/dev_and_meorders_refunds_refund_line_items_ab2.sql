{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_refunds_refund_line_items_ab1') }}
select
    _airbyte_refunds_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast(quantity as {{ dbt_utils.type_bigint() }}) as quantity,
    cast(subtotal as {{ dbt_utils.type_float() }}) as subtotal,
    cast(line_item as {{ type_json() }}) as line_item,
    cast(total_tax as {{ dbt_utils.type_float() }}) as total_tax,
    cast(location_id as {{ dbt_utils.type_bigint() }}) as location_id,
    cast(line_item_id as {{ dbt_utils.type_bigint() }}) as line_item_id,
    cast(restock_type as {{ dbt_utils.type_string() }}) as restock_type,
    cast(subtotal_set as {{ type_json() }}) as subtotal_set,
    cast(total_tax_set as {{ type_json() }}) as total_tax_set,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_refund_line_items_ab1') }}
-- refund_line_items at dev_and_meorders/refunds/refund_line_items
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

