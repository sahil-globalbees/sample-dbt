{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_discount_codes_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        'code',
        adapter.quote('type'),
        'amount',
    ]) }} as _airbyte_discount_codes_hashid,
    tmp.*
from {{ ref('dev_and_meorders_discount_codes_ab2') }} tmp
-- discount_codes at dev_and_meorders/discount_codes
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

