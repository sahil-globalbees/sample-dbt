{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_ref___discount_allocations_ab1') }}
select
    _airbyte_line_item_hashid,
    cast(amount as {{ dbt_utils.type_string() }}) as amount,
    cast(amount_set as {{ type_json() }}) as amount_set,
    cast(discount_application_index as {{ dbt_utils.type_float() }}) as discount_application_index,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_ref___discount_allocations_ab1') }}
-- discount_allocations at dev_and_meorders/refunds/refund_line_items/line_item/discount_allocations
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

