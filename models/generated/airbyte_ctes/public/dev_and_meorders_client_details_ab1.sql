{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar('client_details', ['browser_ip'], ['browser_ip']) }} as browser_ip,
    {{ json_extract_scalar('client_details', ['user_agent'], ['user_agent']) }} as user_agent,
    {{ json_extract_scalar('client_details', ['session_hash'], ['session_hash']) }} as session_hash,
    {{ json_extract_scalar('client_details', ['browser_width'], ['browser_width']) }} as browser_width,
    {{ json_extract_scalar('client_details', ['browser_height'], ['browser_height']) }} as browser_height,
    {{ json_extract_scalar('client_details', ['accept_language'], ['accept_language']) }} as accept_language,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- client_details at dev_and_meorders/client_details
where 1 = 1
and client_details is not null
{{ incremental_clause('_airbyte_emitted_at') }}

