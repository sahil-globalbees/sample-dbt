{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_fulfillments_2ed_price_set_ab1') }}
select
    _airbyte_tax_lines_hashid,
    cast(shop_money as {{ type_json() }}) as shop_money,
    cast(presentment_money as {{ type_json() }}) as presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_fulfillments_2ed_price_set_ab1') }}
-- price_set at dev_and_meorders/fulfillments/line_items/tax_lines/price_set
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

