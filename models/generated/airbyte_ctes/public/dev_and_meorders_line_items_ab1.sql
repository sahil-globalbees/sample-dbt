{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
{{ unnest_cte(ref('dev_and_meorders'), 'dev_and_meorders', 'line_items') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('line_items'), ['sku'], ['sku']) }} as sku,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['name'], ['name']) }} as {{ adapter.quote('name') }},
    {{ json_extract_scalar(unnested_column_value('line_items'), ['grams'], ['grams']) }} as grams,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['price'], ['price']) }} as price,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['title'], ['title']) }} as title,
    {{ json_extract_array(unnested_column_value('line_items'), ['duties'], ['duties']) }} as duties,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['vendor'], ['vendor']) }} as vendor,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['taxable'], ['taxable']) }} as taxable,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['quantity'], ['quantity']) }} as quantity,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['gift_card'], ['gift_card']) }} as gift_card,
    {{ json_extract('', unnested_column_value('line_items'), ['price_set'], ['price_set']) }} as price_set,
    {{ json_extract_array(unnested_column_value('line_items'), ['tax_lines'], ['tax_lines']) }} as tax_lines,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['product_id'], ['product_id']) }} as product_id,
    {{ json_extract_array(unnested_column_value('line_items'), ['properties'], ['properties']) }} as properties,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['variant_id'], ['variant_id']) }} as variant_id,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['pre_tax_price'], ['pre_tax_price']) }} as pre_tax_price,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['variant_title'], ['variant_title']) }} as variant_title,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['product_exists'], ['product_exists']) }} as product_exists,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['total_discount'], ['total_discount']) }} as total_discount,
    {{ json_extract('', unnested_column_value('line_items'), ['origin_location'], ['origin_location']) }} as origin_location,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['requires_shipping'], ['requires_shipping']) }} as requires_shipping,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['fulfillment_status'], ['fulfillment_status']) }} as fulfillment_status,
    {{ json_extract('', unnested_column_value('line_items'), ['total_discount_set'], ['total_discount_set']) }} as total_discount_set,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['fulfillment_service'], ['fulfillment_service']) }} as fulfillment_service,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['admin_graphql_api_id'], ['admin_graphql_api_id']) }} as admin_graphql_api_id,
    {{ json_extract('', unnested_column_value('line_items'), ['destination_location'], ['destination_location']) }} as destination_location,
    {{ json_extract_array(unnested_column_value('line_items'), ['discount_allocations'], ['discount_allocations']) }} as discount_allocations,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['fulfillable_quantity'], ['fulfillable_quantity']) }} as fulfillable_quantity,
    {{ json_extract_scalar(unnested_column_value('line_items'), ['variant_inventory_management'], ['variant_inventory_management']) }} as variant_inventory_management,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- line_items at dev_and_meorders/line_items
{{ cross_join_unnest('dev_and_meorders', 'line_items') }}
where 1 = 1
and line_items is not null
{{ incremental_clause('_airbyte_emitted_at') }}

