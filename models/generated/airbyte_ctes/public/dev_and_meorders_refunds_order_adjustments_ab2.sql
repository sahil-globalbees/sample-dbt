{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_refunds_order_adjustments_ab1') }}
select
    _airbyte_refunds_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_bigint() }}) as {{ adapter.quote('id') }},
    cast(kind as {{ dbt_utils.type_string() }}) as kind,
    cast(amount as {{ dbt_utils.type_string() }}) as amount,
    cast(reason as {{ dbt_utils.type_string() }}) as reason,
    cast(order_id as {{ dbt_utils.type_bigint() }}) as order_id,
    cast(refund_id as {{ dbt_utils.type_bigint() }}) as refund_id,
    cast(amount_set as {{ type_json() }}) as amount_set,
    cast(tax_amount as {{ dbt_utils.type_string() }}) as tax_amount,
    cast(tax_amount_set as {{ type_json() }}) as tax_amount_set,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_order_adjustments_ab1') }}
-- order_adjustments at dev_and_meorders/refunds/order_adjustments
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

