{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_refunds_ref_f40_shop_money_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_price_set_hashid',
        'amount',
        'currency_code',
    ]) }} as _airbyte_shop_money_hashid,
    tmp.*
from {{ ref('dev_and_meorders_refunds_ref_f40_shop_money_ab2') }} tmp
-- shop_money at dev_and_meorders/refunds/refund_line_items/line_item/price_set/shop_money
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

