{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_ful__nts_line_items_duties_ab1') }}
select
    _airbyte_line_items_hashid,
    cast({{ adapter.quote('id') }} as {{ dbt_utils.type_string() }}) as {{ adapter.quote('id') }},
    tax_lines,
    cast(shop_money as {{ type_json() }}) as shop_money,
    cast(presentment_money as {{ type_json() }}) as presentment_money,
    cast(admin_graphql_api_id as {{ dbt_utils.type_string() }}) as admin_graphql_api_id,
    cast(country_code_of_origin as {{ dbt_utils.type_string() }}) as country_code_of_origin,
    cast(harmonized_system_code as {{ dbt_utils.type_string() }}) as harmonized_system_code,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_ful__nts_line_items_duties_ab1') }}
-- duties at dev_and_meorders/fulfillments/line_items/duties
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

