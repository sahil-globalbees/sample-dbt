{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_refunds') }}
{{ unnest_cte(ref('dev_and_meorders_refunds'), 'refunds', 'order_adjustments') }}
select
    _airbyte_refunds_hashid,
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['kind'], ['kind']) }} as kind,
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['amount'], ['amount']) }} as amount,
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['reason'], ['reason']) }} as reason,
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['order_id'], ['order_id']) }} as order_id,
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['refund_id'], ['refund_id']) }} as refund_id,
    {{ json_extract('', unnested_column_value('order_adjustments'), ['amount_set'], ['amount_set']) }} as amount_set,
    {{ json_extract_scalar(unnested_column_value('order_adjustments'), ['tax_amount'], ['tax_amount']) }} as tax_amount,
    {{ json_extract('', unnested_column_value('order_adjustments'), ['tax_amount_set'], ['tax_amount_set']) }} as tax_amount_set,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds') }} as table_alias
-- order_adjustments at dev_and_meorders/refunds/order_adjustments
{{ cross_join_unnest('refunds', 'order_adjustments') }}
where 1 = 1
and order_adjustments is not null
{{ incremental_clause('_airbyte_emitted_at') }}

