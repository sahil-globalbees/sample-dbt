{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_ful__items_origin_location_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_line_items_hashid',
        adapter.quote('id'),
        'zip',
        'city',
        adapter.quote('name'),
        'address1',
        'address2',
        'country_code',
        'province_code',
    ]) }} as _airbyte_origin_location_hashid,
    tmp.*
from {{ ref('dev_and_meorders_ful__items_origin_location_ab2') }} tmp
-- origin_location at dev_and_meorders/fulfillments/line_items/origin_location
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

