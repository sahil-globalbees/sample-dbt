{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_ref__ne_items_subtotal_set_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_refund_line_items_hashid',
        'shop_money',
        'presentment_money',
    ]) }} as _airbyte_subtotal_set_hashid,
    tmp.*
from {{ ref('dev_and_meorders_ref__ne_items_subtotal_set_ab2') }} tmp
-- subtotal_set at dev_and_meorders/refunds/refund_line_items/subtotal_set
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

