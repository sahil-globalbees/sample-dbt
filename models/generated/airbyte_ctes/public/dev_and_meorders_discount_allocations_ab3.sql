{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_discount_allocations_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        adapter.quote('id'),
        'amount',
        'amount_set',
        'created_at',
        'description',
        'application_type',
        'discount_application_index',
    ]) }} as _airbyte_discount_allocations_hashid,
    tmp.*
from {{ ref('dev_and_meorders_discount_allocations_ab2') }} tmp
-- discount_allocations at dev_and_meorders/discount_allocations
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

