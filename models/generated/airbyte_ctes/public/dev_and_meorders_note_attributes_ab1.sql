{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
{{ unnest_cte(ref('dev_and_meorders'), 'dev_and_meorders', 'note_attributes') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar(unnested_column_value('note_attributes'), ['name'], ['name']) }} as {{ adapter.quote('name') }},
    {{ json_extract_scalar(unnested_column_value('note_attributes'), ['value'], ['value']) }} as {{ adapter.quote('value') }},
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- note_attributes at dev_and_meorders/note_attributes
{{ cross_join_unnest('dev_and_meorders', 'note_attributes') }}
where 1 = 1
and note_attributes is not null
{{ incremental_clause('_airbyte_emitted_at') }}

