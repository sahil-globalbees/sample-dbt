{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_shipping_lines') }}
{{ unnest_cte(ref('dev_and_meorders_shipping_lines'), 'shipping_lines', 'discount_allocations') }}
select
    _airbyte_shipping_lines_hashid,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['amount'], ['amount']) }} as amount,
    {{ json_extract('', unnested_column_value('discount_allocations'), ['amount_set'], ['amount_set']) }} as amount_set,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['created_at'], ['created_at']) }} as created_at,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['description'], ['description']) }} as description,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['application_type'], ['application_type']) }} as application_type,
    {{ json_extract_scalar(unnested_column_value('discount_allocations'), ['discount_application_index'], ['discount_application_index']) }} as discount_application_index,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_shipping_lines') }} as table_alias
-- discount_allocations at dev_and_meorders/shipping_lines/discount_allocations
{{ cross_join_unnest('shipping_lines', 'discount_allocations') }}
where 1 = 1
and discount_allocations is not null
{{ incremental_clause('_airbyte_emitted_at') }}

