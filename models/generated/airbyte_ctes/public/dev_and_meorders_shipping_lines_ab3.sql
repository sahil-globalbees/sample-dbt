{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_and_meorders_shipping_lines_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        '_airbyte_dev_and_meorders_hashid',
        adapter.quote('id'),
        'code',
        'phone',
        'price',
        'title',
        adapter.quote('source'),
        'price_set',
        array_to_string('tax_lines'),
        'discounted_price',
        'delivery_category',
        'carrier_identifier',
        array_to_string('discount_allocations'),
        'discounted_price_set',
        'requested_fulfillment_service_id',
    ]) }} as _airbyte_shipping_lines_hashid,
    tmp.*
from {{ ref('dev_and_meorders_shipping_lines_ab2') }} tmp
-- shipping_lines at dev_and_meorders/shipping_lines
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

