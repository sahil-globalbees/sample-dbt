{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders') }}
{{ unnest_cte(ref('dev_and_meorders'), 'dev_and_meorders', 'refunds') }}
select
    _airbyte_dev_and_meorders_hashid,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['id'], ['id']) }} as {{ adapter.quote('id') }},
    {{ json_extract_scalar(unnested_column_value('refunds'), ['note'], ['note']) }} as note,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['restock'], ['restock']) }} as restock,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['user_id'], ['user_id']) }} as user_id,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['order_id'], ['order_id']) }} as order_id,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['created_at'], ['created_at']) }} as created_at,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['processed_at'], ['processed_at']) }} as processed_at,
    {{ json_extract_array(unnested_column_value('refunds'), ['transactions'], ['transactions']) }} as transactions,
    {{ json_extract_array(unnested_column_value('refunds'), ['order_adjustments'], ['order_adjustments']) }} as order_adjustments,
    {{ json_extract_array(unnested_column_value('refunds'), ['refund_line_items'], ['refund_line_items']) }} as refund_line_items,
    {{ json_extract_scalar(unnested_column_value('refunds'), ['admin_graphql_api_id'], ['admin_graphql_api_id']) }} as admin_graphql_api_id,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders') }} as table_alias
-- refunds at dev_and_meorders/refunds
{{ cross_join_unnest('dev_and_meorders', 'refunds') }}
where 1 = 1
and refunds is not null
{{ incremental_clause('_airbyte_emitted_at') }}

