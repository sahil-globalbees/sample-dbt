{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ ref('dev_and_meorders_refunds_order_adjustments') }}
select
    _airbyte_order_adjustments_hashid,
    {{ json_extract('table_alias', 'tax_amount_set', ['shop_money'], ['shop_money']) }} as shop_money,
    {{ json_extract('table_alias', 'tax_amount_set', ['presentment_money'], ['presentment_money']) }} as presentment_money,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_refunds_order_adjustments') }} as table_alias
-- tax_amount_set at dev_and_meorders/refunds/order_adjustments/tax_amount_set
where 1 = 1
and tax_amount_set is not null
{{ incremental_clause('_airbyte_emitted_at') }}

