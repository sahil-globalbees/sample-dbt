{{ config(
    indexes = [{'columns':['_airbyte_emitted_at'],'type':'btree'}],
    schema = "_airbyte_public",
    tags = [ "nested-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('dev_and_meorders_client_details_ab1') }}
select
    _airbyte_dev_and_meorders_hashid,
    cast(browser_ip as {{ dbt_utils.type_string() }}) as browser_ip,
    cast(user_agent as {{ dbt_utils.type_string() }}) as user_agent,
    cast(session_hash as {{ dbt_utils.type_string() }}) as session_hash,
    cast(browser_width as {{ dbt_utils.type_bigint() }}) as browser_width,
    cast(browser_height as {{ dbt_utils.type_bigint() }}) as browser_height,
    cast(accept_language as {{ dbt_utils.type_string() }}) as accept_language,
    _airbyte_ab_id,
    _airbyte_emitted_at,
    {{ current_timestamp() }} as _airbyte_normalized_at
from {{ ref('dev_and_meorders_client_details_ab1') }}
-- client_details at dev_and_meorders/client_details
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

